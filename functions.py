import json
import gitlab

def licenseIsAllowed(license, allowedLicenses):
    return license in allowedLicenses

def getLicencesFile(project_id):
    gl = gitlab.Gitlab('https://gitlab.com')
    project = gl.projects.get(project_id)
    file = project.files.get(file_path='allowedLicenses.json', ref='main')
    obj = json.loads(file.decode())
    return obj

def validateSPDX(pathToSbom):
    
    with open(pathToSbom, 'r') as sbomFile:
        sbom = json.load(sbomFile)

    allowedLicenses = getLicencesFile(47788142)

    packages = sbom['packages']
    packagesWithIllegalLicense = []

    for package in packages:
        if not licenseIsAllowed(package['licenseDeclared'], allowedLicenses):
            packagesWithIllegalLicense.append(package)

    if len(packagesWithIllegalLicense) == 0:
        print("All licences permitted.")
    else:
        listForException = []
        for package in packagesWithIllegalLicense:
            obj = {}
            obj['packageName'] = package['name']
            obj['license'] = package['licenseDeclared']
            listForException.append(obj)
        listAsString = str(listForException)
        raise Exception("Not allowed licence(s) found!\n" + listAsString)