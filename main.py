from functions import validateSPDX
import argparse

parser = argparse.ArgumentParser(description="Validate Software Bill of Materials in SPDX format.")
parser.add_argument("pathToSbom", type=str)

args = parser.parse_args()

validateSPDX(args.pathToSbom)   

