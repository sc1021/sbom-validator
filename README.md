# sbom-validator

This project contains a simple tool that validates the open source licences in a spdx sbom (Software Bill of Materials). The matching happens with the permitted licences from [opencode.de](https://wikijs.opencode.de/de/Hilfestellungen_und_Richtlinien/Lizenzcompliance).

## Usage

Call main.py together with the path to the sbom that should be validated.

```
python3 main.py <pathToSbom>
```

If all found licensen comply with the specification, the console prints: `All licences permitted.`

If at least one license is not allowed, an exception is raised that lists the corresponding packages.

This exception is useful, when validating a project's sbom in a CI/CD pipeline, because the exception makes the pipeline fail.
